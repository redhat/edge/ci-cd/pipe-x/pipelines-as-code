variables:
  WORKSPACE_ID: "${CI_PIPELINE_ID}.${CI_COMMIT_SHORT_SHA}"
  CI_REPOS_ENDPOINT: "https://${S3_BUCKET_WORKSPACES}.s3.${AWS_DEFAULT_REGION}.amazonaws.com"

deploy-gitlab-ci-branch:
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tools-container:latest"
  script:
    - set -euo pipefail
    - echo "Installing netrc"
    - echo "machine ${CI_SERVER_HOST} login git password ${PIPELINE_GIT_TOKEN}" > ~/.netrc
    - echo "Mirroring main branch to gitlab-ci branch"
    - git clone "${CI_PROJECT_URL}" pipeline-repo
    - cd pipeline-repo
    - git push origin main:gitlab-ci
  stage: .pre
  rules:
    - !reference [.run_on_merge]

initiate-workspace:
  stage: init
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tools-container:latest"
  script:
    - |-
      # Prepare shared environment variables in pipeline
      DATE=$(date -d "${CI_PIPELINE_CREATED_AT}" +"%Y%m%d%H%M")
      RELEASE_ID="${RELEASE_NAME:-${RELEASE}}-${DATE}"
      UPSTREAM_DISTRO="${UPSTREAM_DISTRO_ID}-${UPSTREAM_DISTRO_VERSION}"

      # Add the calculated variables to the dotenv file for other jobs to use them
      echo "DATE=$DATE" >> pipeline.env
      echo "RELEASE_ID=$RELEASE_ID" >> pipeline.env
      echo "UPSTREAM_DISTRO=$UPSTREAM_DISTRO" >> pipeline.env

      # Prepare info.txt
      echo "Started: $CI_JOB_STARTED_AT" | tee info.txt
      echo "Project: $CI_PROJECT_PATH" | tee -a info.txt
      echo "Branch: $CI_COMMIT_BRANCH" | tee -a info.txt
      echo "Commit: $CI_COMMIT_SHA" | tee -a info.txt
      echo "Release ID: $RELEASE_ID" | tee -a info.txt
      echo "Upstream distro: $UPSTREAM_DISTRO" | tee -a info.txt
      echo "UUID: $WORKSPACE_ID" | tee -a info.txt
      echo "Config REF: $CONFIG_YAML_REF" | tee -a info.txt
      echo "Pungi REF: $PUNGI_CONFIG_REPO_REF" | tee -a info.txt
      echo "OSBUILD REF: $OSBUILD_TMT_REPO_REF" | tee -a info.txt
      echo "Custom Images REF: $CUSTOM_IMAGES_REF" | tee -a info.txt
      echo "Pipelines-as-code REF: $PIPELINE_REPO_REF" | tee -a info.txt
      echo "Pipeline_URL: $CI_PIPELINE_URL" | tee -a info.txt
      echo "Pipeline_ID: $CI_PIPELINE_ID" | tee -a info.txt
      if [ ! -z "$CI_MERGE_REQUEST_ID" ]; then
        echo "--" | tee -a info.txt
        echo "# $CI_MERGE_REQUEST_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID" | tee -a info.txt
      fi
      if [ -v UPSTREAM_PROJECT ]; then
        echo "--" | tee -a info.txt
        echo "Parent project: $UPSTREAM_PROJECT" | tee -a info.txt
        echo "Parent branch: $UPSTREAM_BRANCH" | tee -a info.txt
        echo "Parent tag: $UPSTREAM_COMMIT_TAG" | tee -a info.txt
      fi

      # If working without secrets, exit without uploading.
      if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
        echo "Your aws-access-key-id or aws-secret-access-key are missing. Exiting without upload.."
        exit 1
      fi
      echo "Uploading..."
      aws s3 cp info.txt "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}/"
  artifacts:
    reports:
      dotenv: pipeline.env
  rules:
    - if: $DISABLE_INITIATE_WORKSPACE == "true"
      when: never
    - !reference [.run_always]

generate-compose:
  stage: build
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/pungi:latest"
  before_script:
    - |-
      # Define log level (default INFO)
      export LOG_LEVEL="INFO"
      # Enable timestamps and log level
      export PS4='+[$(date "+%Y-%m-%d %H:%M:%S")] [$LOG_LEVEL] '
      # Trap to change log level to ERROR when a command fails
      trap 'export LOG_LEVEL="ERROR"; export PS4="+[$(date "+%Y-%m-%d %H:%M:%S")] [ERROR] "' ERR
      set -x
      if [[ -v BASE_URL ]] && [[ -n "${BASE_URL}" ]]; then
        echo "Don't build the compose and use '${BASE_URL}' instead"
        exit 0
      fi
  script:
    - !reference [.functions]
    - |-
      #!/usr/bin/env bash
      set -euxo pipefail

      clone_repo_and_cd "${PUNGI_CONFIG_REPO}" "${PUNGI_CONFIG_REPO_REF}"
      cd pungi-config
      mkdir -p autosd autosd-cbs autosd-cs9 autosd-centos upload $CI_PROJECT_DIR/packages
      mv koji.conf.d/* /etc/koji.conf.d/
      # Gather packages and builds that are in compose
      TAG=$(sed -n '/pkgset_koji_tag =/ s/.*"\([^"]*\)".*/\1/p' autosd-cbs.conf)
      koji --profile=cbs list-pkgs --tag="$TAG" | awk 'NR > 2 {print $1}' > upload/pkgs.txt
      koji --profile=cbs list-tagged "$TAG" --latest | awk 'NR > 2 {print $1}' > upload/builds.txt
      cp upload/*.txt $CI_PROJECT_DIR/packages/
      echo "Generate CBS Compose"
      pungi-koji --target-dir=autosd-cbs --config=autosd-cbs.conf --test
      echo "Generate CS9 Compose"
      pungi-koji --target-dir=autosd-cs9 --config=autosd-cs9.conf --test
      echo "Generate AutoSD Centos Compose"
      pungi-koji --target-dir=autosd-centos --config=autosd-centos.conf --test
      echo "Generate AutoSD Centos Compose"
      pungi-koji --target-dir=autosd --config=autosd.conf --test
      echo "Uploading"
      mv autosd/AutoSD-*/* upload
      # Strip pungi downloads from workdir to reduce upload size
      # Can save over 50GiB of wasted disk space / bandwidth
      rm -rf upload/work/aarch64/pungi_download
      rm -rf upload/work/global/download
      rm -rf upload/work/x86_64/pungi_download
      aws s3 cp upload "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}/repos/AutoSD" --recursive --only-show-errors
  artifacts:
    when: always
    paths:
      - packages/*.txt
  retry: 2
  rules:
    - if: $DISABLE_GENERATE_COMPOSE == "true"
      when: never
    - !reference [.run_always]

build-and-publish-builder:
  image: quay.io/testing-farm/cli:v0.0.21-0e5eb7e9
  stage: test
  needs: ["generate-compose"]
  variables:
    TMT_PLAN: ^/plans/publish-env$
    BASE_URL: "${WEBSERVER_WORKSPACES}/${WORKSPACE_ID}"
  parallel:
    matrix:
      - ARCH:
        - x86_64
        - aarch64
  before_script:
    - |-
      set -x
      if ! [[ "${RELEASE}" =~ (nightly|latest*) ]]; then
        echo "Only build the builder container image for nightly builds"
        exit 0
      fi
  script:
    - |-
      set -x
      export TESTING_FARM_API_TOKEN="${TF_API_KEY}"
      testing-farm request --arch "${ARCH}" \
                           --compose "${TF_COMPOSE}" \
                           --git-url "${OSBUILD_TMT_REPO}" \
                           --git-ref "${OSBUILD_TMT_REPO_REF}" \
                           --plan "${TMT_PLAN}" \
                           -e STREAM="${STREAM}" \
                           -e BASE_URL="${BASE_URL}" \
                           -e CONTAINER_REGISTRY="${CONTAINER_REGISTRY}" \
                           -e CONTAINER_REGISTRY_NAMESPACE="${CONTAINER_REGISTRY_NAMESPACE}" \
                           -e UUID="${WORKSPACE_ID}" \
                           -s CONTAINER_REGISTRY_USER="${CONTAINER_REGISTRY_USER}" \
                           -s CONTAINER_REGISTRY_PASSWORD="${CONTAINER_REGISTRY_PASSWORD}"
  rules:
    - if: $DISABLE_BUILD_AND_PUBLISH_BUILDER == "true"
      when: never
    - !reference [.run_on_schedule]

create-core-rpms-list:
  stage: test
  extends: .create-osbuild
  needs: ["generate-compose"]
  variables:
    S3_UPLOAD_DIR: "core-rpms"
    TMT_PLAN: ^/plans/create-core-rpms$
    BUILD_FORMAT: txt
  parallel:
    matrix:
      - ARCH:
        - x86_64
        - aarch64
  retry: 2
  rules:
    - if: $DISABLE_CREATE_CORE_RPMS_LIST == "true"
      when: never
    - !reference [.run_always]

.create-osbuild:
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tf-requests:${TF_REQUESTS_TAG}"
  needs: ["initiate-workspace"]
  variables:
    ACTION: BUILD
    BUILD_TYPE: osbuild
    BUILD_TARGET: qemu
    BUILD_FORMAT: qcow2
    IMAGE_NAME: minimal
    IMAGE_TYPE: ostree
    TEST_IMAGE: "no"
    SET_IMAGE_SIZE: "no"
    PACKAGE_SET: "${PRODUCT_BUILD_PREFIX}"
    TMT_PLAN: ^/plans/create-osbuild$
    AWS_REGION: "${AWS_DEFAULT_REGION}"
    UUID: "${WORKSPACE_ID}"
    OSTREE_REPO: "no"
  script:
    - |-
      set -x
      # Trigger by the sample-iamge repo
      if [[ "${CI_PIPELINE_SOURCE}" == "pipeline" ]] && [[ "$UPSTREAM_PROJECT" == "$SAMPLE_IMAGES_PROJECT" ]]; then
          SAMPLE_IMAGES_REF="${UPSTREAM_REF}"
          #TODO: remove after updated on create-osbuild
          REVISION="${UPSTREAM_REF}"
      fi
      # Trigger by a fork of the sample-images repo
      if [ -n "$UPSTREAM_FORK_REPO" ] && [[ "$UPSTREAM_PROJECT" == "$SAMPLE_IMAGES_PROJECT" ]]; then
          SAMPLE_IMAGES_REPO="${UPSTREAM_FORK_REPO}"
          #TODO: remove after updated on create-osbuild
          REPO_URL="${UPSTREAM_FORK_REPO}"
      #TODO: remove after updated on create-osbuild
      # Trigger by the a branch at the sample-images repo
      elif [ -v UPSTREAM_BRANCH ] && [[ "$UPSTREAM_PROJECT" == "$SAMPLE_IMAGES_PROJECT" ]]; then
          REPO_URL="${SAMPLE_IMAGES_REPO}"
      # Trigger by the pipeline-as-code or the autosd repo
      else
          REPO_URL="${SAMPLE_IMAGES_REPO}"
          REVISION="${SAMPLE_IMAGES_REF}"
      fi
      export SAMPLE_IMAGES_REPO
      export SAMPLE_IMAGES_REF
      #TODO: remove after updated on create-osbuild
      export REPO_URL
      export REVISION

      # Trigger by the create-osbuild repo
      if [[ "${CI_PIPELINE_SOURCE}" == "pipeline" ]] && [[ "$UPSTREAM_PROJECT" == "$OSBUILD_TMT_PROJECT" ]]; then
          # We use UPSTREAM_BRANCH instead of UPSTREAM_REF, because Testing Farm requires
          # a full sha-1 hash, but UPSTREAM_REF is the short one.
          CI_REF="${UPSTREAM_BRANCH}"
      fi
      # Trigger by a fork of the create-osbuild repo
      if [ -n "$UPSTREAM_FORK_REPO" ] && [[ "$UPSTREAM_PROJECT" == "$OSBUILD_TMT_PROJECT" ]]; then
          CI_REPO_URL="${UPSTREAM_FORK_REPO}"
      # Trigger by the pipeline-as-code or the autosd repo
      else
          CI_REPO_URL="${OSBUILD_TMT_REPO}"
          CI_REF="${OSBUILD_TMT_REPO_REF}"
      fi
      export CI_REPO_URL
      export CI_REF

      # Trigger by the custom-images repo
      if [[ "${CI_PIPELINE_SOURCE}" == "pipeline" ]] && [[ "$UPSTREAM_PROJECT" == "$CUSTOM_IMAGES_PROJECT" ]]; then
          CUSTOM_IMAGES_REF="${UPSTREAM_REF}"
      fi
      # Trigger by a fork of the custom-images repo
      if [ -n "$UPSTREAM_FORK_REPO" ] && [[ "$UPSTREAM_PROJECT" == "$CUSTOM_IMAGES_PROJECT" ]]; then
          CUSTOM_IMAGES_REPO="${UPSTREAM_FORK_REPO}"
      fi
      export CUSTOM_IMAGES_REPO
      export CUSTOM_IMAGES_REF

      # Trigger by the automotive-image-builder repo
      if [[ "${CI_PIPELINE_SOURCE}" == "pipeline" ]] && [[ "$UPSTREAM_PROJECT" == "$AIB_PROJECT" ]]; then
          AIB_REF="${UPSTREAM_REF}"
      fi
      # Trigger by a fork of the automotive-image-builder repo
      if [ -n "$UPSTREAM_FORK_REPO" ] && [[ "$UPSTREAM_PROJECT" == "$AIB_PROJECT" ]]; then
          AIB_REPO="${UPSTREAM_FORK_REPO}"
      fi
      export AIB_REPO
      export AIB_REF

      if [[ -v WEBSERVER_WORKSPACES ]]; then
        export CI_REPOS_ENDPOINT="${WEBSERVER_WORKSPACES}"
      fi
      export IMAGE_KEY="auto-${BUILD_TYPE}-${BUILD_TARGET}-${PACKAGE_SET}-${IMAGE_NAME}-${IMAGE_TYPE}-${ARCH}-${WORKSPACE_ID}"
      tf-requests --variables 'ACTION,TF_COMPOSE,USE_AIB_RPM,X_STREAM,TMT_PLAN,CI_REF,CI_REPO_URL,ARCH,AWS_CKI_REGION,AWS_REGION,AWS_TF_REGION,BUILD_FORMAT,BUILD_TARGET,BUILD_TYPE,CI_REPOS_ENDPOINT,COMPOSE_DISTRO,CUSTOM_IMAGES_REF,CUSTOM_IMAGES_REPO,IMAGE_KEY,IMAGE_NAME,IMAGE_TYPE,OS_OPTIONS,OS_PREFIX,OS_VERSION,PACKAGE_SET,PIPELINE_REPO,PRODUCT_BUILD_PREFIX,RELEASE,REPO_URL,REVISION,S3_BUCKET_NAME,SAMPLE_IMAGE,SAMPLE_IMAGES_REPO,IMAGE_SIZE,SET_IMAGE_SIZE,SSH_KEY,SSL_VERIFY,STREAM,TEST_IMAGE,UUID,WEBSERVER_RELEASES,WEBSERVER_WORKSPACES,WORKSPACE_ID,S3_UPLOAD_DIR,IMPORT_IMAGE,UEFI_VENDOR,OSTREE_OS_NAME,DOWNSTREAM_COMPOSE_URL,OSTREE_REPO,NIGHTLY,BASE_URL,USE_MODULE_SIG_ENFORCE,UPDATE_OSTREE_REPO,RELEASE_NAME,DATE,RELEASE_ID,UPSTREAM_DISTRO,SAMPLE_IMAGES_REPO,SAMPLE_IMAGES_REF,AIB_REPO,AIB_REF,AUTOMOTIVE_OSBUILD_IMAGE,AUTOMOTIVE_OSBUILD_TAG,USE_LATEST,NIGHTLY_COMPOSE,CONTAINER_REGISTRY,CONTAINER_REGISTRY_NAMESPACE,ENABLE_FUSA' \
      --secrets 'AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,AWS_CKI_ACCOUNT_ID,AWS_TF_ACCOUNT_ID,CONTAINER_REGISTRY_USER,CONTAINER_REGISTRY_PASSWORD'
      source pipeline.env
      if [[ "$RESULT" != "passed" ]]; then
        exit 1
      fi
    - !reference [.check_artifacts]
  retry: 2

# Job template for building images for boards
.build-board-image:
  stage: test
  extends: .create-osbuild
  needs: ["initiate-workspace", "generate-compose"]
  variables:
    IMAGE_NAME: qa
    SAMPLE_IMAGE: "no"
    ARCH: aarch64
    BUILD_FORMAT: img
    IMAGE_TYPE: regular
    TEST_IMAGE: "yes"
    environments__variables__ENABLE_SSH_PASSWORD_AUTH: "true"

# Images for boards
build-TI-am69sk-image:
  extends: .build-board-image
  variables:
    BUILD_TARGET: am69sk
    S3_UPLOAD_DIR: TI
  rules:
    - if: $DISABLE_TI_AM69SK_IMAGE == "true"
      when: never
    - !reference [.run_always]

build-TI-j784s4evm-image:
  extends: .build-board-image
  variables:
    BUILD_TARGET: j784s4evm
    S3_UPLOAD_DIR: TI
  rules:
    - if: $DISABLE_TI_J784S4EVM_IMAGE == "true"
      when: never
    - !reference [.run_always]

build-NXP-S32G-image:
  extends: .build-board-image
  variables:
    BUILD_TARGET: s32g_vnp_rdb3
    S3_UPLOAD_DIR: NXP
  rules:
    - if: $DISABLE_NXP_S32G_IMAGE == "true"
      when: never
    - !reference [.run_always]

build-RideSX4-image:
  extends: .build-board-image
  variables:
    BUILD_TARGET: ridesx4
    BUILD_FORMAT: aboot.simg
    S3_UPLOAD_DIR: RideSX4
  parallel:
    matrix:
      - IMAGE_TYPE: regular
      - IMAGE_TYPE: ostree
  rules:
    - if: $DISABLE_RIDESX4_IMAGE == "true"
      when: never
    - !reference [.run_always]

build-Renesas-S4-image:
  extends: .build-board-image
  variables:
    BUILD_TARGET: rcar_s4
    S3_UPLOAD_DIR: Renesas
  parallel:
    matrix:
      - IMAGE_TYPE: regular
      - IMAGE_TYPE: ostree
  rules:
    - if: $DISABLE_RENESAS_S4_IMAGE == "true"
      when: never
    - !reference [.run_always]

# Images for automatic testing in the pipeline
build-testing-images:
  stage: test
  extends: .create-osbuild
  needs: ["initiate-workspace", "generate-compose"]
  variables:
    BUILD_TARGET: qemu
    BUILD_FORMAT: img
    SAMPLE_IMAGE: "no"
    TEST_IMAGE: "yes"
    IMPORT_IMAGE: "yes"  # For testing in the pipeline
  parallel:
    matrix:
      - IMAGE_NAME: fusa-minimal
        ENABLE_FUSA: "true"
        ARCH:
          - aarch64
          - x86_64
      - IMAGE_NAME: qa
        IMAGE_TYPE: regular
        ARCH:
          - aarch64
          - x86_64
      - IMAGE_NAME: cki
        BUILD_TARGET: aws
        IMAGE_TYPE: regular
        ARCH:
          - aarch64
          - x86_64
  rules:
    - if: $DISABLE_BUILD_TESTING_IMAGES == "true"
      when: never
    - !reference [.run_always]

# Images to download and try locally. Local testing or debugging
build-sample-images:
  stage: test
  extends: .create-osbuild
  needs: ["initiate-workspace", "generate-compose"]
  variables:
    SAMPLE_IMAGE: "yes"
    BUILD_TARGET: qemu
    BUILD_FORMAT: qcow2
    IMAGE_NAME: developer
  parallel:
    matrix:
      - IMAGE_TYPE: regular
        ARCH:
          - aarch64
          - x86_64
      - IMAGE_TYPE: ostree
        ARCH:
          - aarch64
          - x86_64
      - IMAGE_TYPE: regular
        ARCH: aarch64
        BUILD_TARGET: rpi4
        BUILD_FORMAT: img
  rules:
    - if: $DISABLE_BUILD_SAMPLE_IMAGES == "true"
      when: never
    - !reference [.run_always]

check-denylist-rpms:
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tools-container:latest"
  stage: smoke-test
  needs: ["create-core-rpms-list"]
  script:
    - |-
      #!/usr/bin/env bash
      set -euxo pipefail

      if [[ -v WEBSERVER_WORKSPACES ]]; then
        export CI_REPOS_ENDPOINT="${WEBSERVER_WORKSPACES}"
      fi
      BASE_URL="${CI_REPOS_ENDPOINT}/${WORKSPACE_ID}/core-rpms"
      ARCH=("aarch64" "x86_64")
      DISTRO="${PRODUCT_BUILD_PREFIX//[0-9]/}"
      echo "Downloading core rpms list"
      for arch in "${ARCH[@]}"; do
        wget "${BASE_URL}/${DISTRO}-core-rpms-${arch}.txt"
      done

      for file in $(find . -type f -name '*core-rpms*.txt'); do
        for line in "${DENYLIST[@]}"; do
          if grep -q "${line}" "${file}"; then
            echo "Found denied content in ${file}: ${line}"
            exit 1
          fi
        done
      done
  retry: 2
  allow_failure: true
  rules:
    - if: $DISABLE_CHECK_DENYLIST_RPMS == "true"
      when: never
    - !reference [.run_always]

# template for running tests using images from the pipeline
.image-tests:
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tf-requests:${TF_REQUESTS_TAG}"
  stage: smoke-test
  needs: ["build-testing-images"]
  variables:
    CI_REPO_URL: "${TESTS_BASE_IMAGE_REPO}"
    CI_REF: "${TESTS_BASE_IMAGE_REF}"
    ACTION: TEST
    BUILD_TYPE: osbuild
    BUILD_TARGET: qemu
    TEST_IMAGE: "yes"
    IMAGE_TYPE: ostree
    PACKAGE_SET: "${PRODUCT_BUILD_PREFIX}"
    UUID: "${WORKSPACE_ID}"
    AWS_REGION: "${AWS_DEFAULT_REGION}"
  parallel:
    matrix:
      - ARCH: aarch64
      - ARCH: x86_64
  script:
    - export IMAGE_KEY="auto-${BUILD_TYPE}-${BUILD_TARGET}-${PACKAGE_SET}-${IMAGE_NAME}-${IMAGE_TYPE}-${ARCH}-${WORKSPACE_ID}"
    - tf-requests --variables 'ACTION,TF_COMPOSE,TMT_PLAN,CI_REF,CI_REPO_URL,ARCH,AWS_CKI_REGION,AWS_REGION,AWS_TF_REGION,BUILD_FORMAT,BUILD_TARGET,BUILD_TYPE,CI_REPOS_ENDPOINT,COMPOSE_DISTRO,CUSTOM_IMAGES_REF,CUSTOM_IMAGES_REPO,IMAGE_KEY,IMAGE_NAME,IMAGE_TYPE,OS_OPTIONS,OS_PREFIX,OS_VERSION,PACKAGE_SET,PIPELINE_REPO,PRODUCT_BUILD_PREFIX,RELEASE,REPO_URL,REVISION,S3_BUCKET_NAME,SAMPLE_IMAGE,SAMPLE_IMAGES_REPO,SET_IMAGE_SIZE,SSH_KEY,SSL_VERIFY,STREAM,TEST_IMAGE,UUID,WEBSERVER_RELEASES,WEBSERVER_WORKSPACES,WORKSPACE_ID,OSTREE_OS_NAME' --secrets 'AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY'
    - |-
      source pipeline.env
      if [[ "$RESULT" != "passed" ]]; then
        exit 1
      fi
  retry: 2
  artifacts:
    expose_as: "Tests html report"
    paths: ["tests_results"]

smoke-tests-fusa-minimal:
  extends: .image-tests
  variables:
    IMAGE_NAME: fusa-minimal
    BUILD_FORMAT: img
    IMAGE_TYPE: ostree
    TMT_PLAN: ^/smoke/plans/fusa$
  rules:
    - if: $DISABLE_SMOKE_TESTS_FUSA_MINIMAL == "true"
      when: never
    - !reference [.run_always]

smoke-tests-qa:
  extends: .image-tests
  variables:
    IMAGE_NAME: qa
    BUILD_FORMAT: img
    IMAGE_TYPE: regular
    TMT_PLAN: ^/smoke/plans/core$
  rules:
    - if: $DISABLE_SMOKE_TESTS_QA == "true"
      when: never
    - !reference [.run_always]

environment-details:
  extends: .image-tests
  variables:
    IMAGE_NAME: fusa-minimal
    BUILD_FORMAT: img
    TMT_PLAN: ^/environment/plans/environment_info$
  script:
    - export IMAGE_KEY="auto-${BUILD_TYPE}-${BUILD_TARGET}-${PACKAGE_SET}-${IMAGE_NAME}-${IMAGE_TYPE}-${ARCH}-${WORKSPACE_ID}"
    - tf-requests --variables 'ARCH,IMAGE_KEY,IMAGE_NAME,RELEASE,TF_ENDPOINT,UUID,PRODUCT_BUILD_PREFIX,CI_REPOS_ENDPOINT,COMPOSE_DISTRO,STREAM'
    - |-
      source pipeline.env
      if [[ "$RESULT" != "passed" ]]; then
        exit 1
      fi

      # The 'if' statement is to support the old and new 'tf-requests' which should
      # exports the new var 'ARTIFACTS_WORKDIR_URLS' instead of 'ARTIFACTS_WORKDIR_URL'
      if [ -n "$ARTIFACTS_WORKDIR_URLS" ]; then
        ARTIFACTS_WORKDIR_URL=$(echo "${ARTIFACTS_WORKDIR_URLS}" | jq -r '.[].workdir')
      fi

      ENV_INFO_URL="${ARTIFACTS_WORKDIR_URL}/environment/plans/environment_info/data/environment_information.log"
      REPORT_FILE="environment_information_${IMAGE_NAME}_${IMAGE_TYPE}_${ARCH}.log"
      echo "Trying to download the report from ${ENV_INFO_URL}"
      retry_counter=10
      while (( --retry_counter >= 0 )); do
        if curl --fail --output "${REPORT_FILE}" "${ENV_INFO_URL}" ; then
          break
        fi
        sleep 3
      done
      if [ ! -f "${REPORT_FILE}" ]; then
        exit 1
      fi
      echo "Uploading the environment information log to ${WORKSPACE_ID}..."
      aws s3 cp "${REPORT_FILE}" "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}/environment_info/"
  rules:
    - if: $DISABLE_ENVIRONMENT_DETAILS == "true"
      when: never
    - !reference [.run_always]

upload-images-info:
  stage: report
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/image-info-generator:${IMAGE_INFO_GENERATOR_TAG}"
  script:
    - |
      echo "Gathering images information from the workspace"
      imagen

      echo "Uploading testing images info to ${WORKSPACE_ID}..."
      aws s3 cp test_images_info.json "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}/"
  rules:
    - if: $DISABLE_UPLOAD_IMAGES_INFO == "true"
      when: never
    - !reference [.run_always]

.upload-reports:
  stage: report
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tools-container:latest"
  script:
    - |-
      echo "Uploading test reports to ${WORKSPACE_ID}..."
      aws s3 cp tests_results "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}/integration-testing/${IMAGE_NAME}/${REPORTS_DIR}" --recursive

upload-smoke-tests-report-fusa-minimal:
  extends: .upload-reports
  needs: ["smoke-tests-fusa-minimal"]
  variables:
    REPORTS_DIR: smoke-tests
    IMAGE_NAME: fusa-minimal
  rules:
    - if: $DISABLE_SMOKE_TESTS_FUSA_MINIMAL == "true"
      when: never
    - !reference [.run_always]

upload-smoke-tests-report-qa:
  extends: .upload-reports
  needs: ["smoke-tests-qa"]
  variables:
    REPORTS_DIR: smoke-tests
    IMAGE_NAME: qa
  rules:
    - if: $DISABLE_SMOKE_TESTS_qa == "true"
      when: never
    - !reference [.run_always]

upload-environment-details-report:
  extends: .upload-reports
  needs: ["environment-details"]
  variables:
    REPORTS_DIR: environment-details
    IMAGE_NAME: fusa-minimal
  rules:
    - if: $DISABLE_ENVIRONMENT_DETAILS == "true"
      when: never
    - !reference [.run_always]

promote-product-build-and-release:
  stage: deploy
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tools-container:latest"
  script:
    - |-
      # Creates a fresh build like this: autosd9-202308170203
      echo "Syncing ${PRODUCT_BUILD_PREFIX}-${DATE} build..."
      aws s3 sync --delete \
                "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}" \
                "s3://${S3_BUCKET_RELEASES}/${PRODUCT_BUILD_PREFIX}-${DATE}/"

      # Release, be it a full release or a nightly
      echo "Syncing ${RELEASE} build..."
      aws s3 sync --delete \
                "s3://${S3_BUCKET_WORKSPACES}/${WORKSPACE_ID}" \
                "s3://${S3_BUCKET_RELEASES}/${RELEASE}"
  rules:
    - if: $DISABLE_PROMOTE_PRODUCT_BUILD == "true"
      when: never
    - !reference [.run_on_release_tag]
    - !reference [.run_on_external_merge_request]
    - !reference [.run_on_merge_request]
    - !reference [.run_on_schedule]

promote-builder:
  image: quay.io/buildah/stable:latest
  stage: deploy
  needs: ["build-and-publish-builder", "promote-product-build-and-release"]
  variables:
    UUID: "${WORKSPACE_ID}"
    DISTRO: "autosd"
  before_script:
    - |-
      set -x
      if ! [[ "${RELEASE}" =~ (nightly|latest*) ]]; then
        echo "Only promote the builder container image for nightly builds"
        exit 0
      fi
  script:
    - |-
      set -x
      image_name="${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/${DISTRO}-builder"

      buildah login "${CONTAINER_REGISTRY}" \
                    --username "${CONTAINER_REGISTRY_USER}" \
                    --password "${CONTAINER_REGISTRY_PASSWORD}"

      for arch in aarch64 x86_64; do
        buildah pull "${image_name}:${UUID}-${arch}"
      done

      buildah manifest create "${image_name}:latest" \
                              "${image_name}:${UUID}-aarch64" \
                              "${image_name}:${UUID}-x86_64"

      buildah manifest inspect "${image_name}:latest"

      buildah manifest push "${image_name}:latest"
  rules:
    - if: $DISABLE_PROMOTE_BUILDER == "true"
      when: never
    - !reference [.run_on_schedule]

# Quick check that product-build/nightly-build uploaded some items
selftest-product-build:
  stage: deploy
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/tools-container:latest"
  needs: ["initiate-workspace", "promote-product-build-and-release"]
  script:
    - |-
      # Check that the product-build or nightly build is in the bucket
      # and some contents exist
      FAILURES=0
      while IFS= read -r line; do
        if [ -z "${line}" ]; then
          continue
        fi

        echo "Checking ${S3_BUCKET_RELEASES} for ${PRODUCT_BUILD_PREFIX}-${DATE}/${line}"
        aws s3 sync "s3://${S3_BUCKET_RELEASES}/${PRODUCT_BUILD_PREFIX}-${DATE}/" . \
          --exclude "*" \
          --include "${line}" \
          --dryrun | \
          grep "download" || \
          { echo "Failed to find: '${line}'"; FAILURES=1; }

        echo "Checking ${S3_BUCKET_RELEASES} for ${RELEASE}/${line}"
        aws s3 sync "s3://${S3_BUCKET_RELEASES}/${RELEASE}/" . \
          --exclude "*" \
          --include "${line}" \
          --dryrun | \
          grep "download" || \
          { echo "Failed to find: '${line}'"; FAILURES=1; }
      done <<< "${PRODUCT_BUILD_CHECKS}"

      if [ ${FAILURES} -ne 0 ]; then
        exit 1
      fi
  rules:
    - if: $DISABLE_SELFTEST == "true"
      when: never
    - !reference [.run_on_release_tag]
    - !reference [.run_on_external_merge_request]
    - !reference [.run_on_merge_request]
    - !reference [.run_on_schedule]

promote-koji-builds:
  stage: deploy
  image: "${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/pungi:latest"
  needs: ["selftest-product-build", "generate-compose"]
  script:
    - !reference [.functions]
    - |-
      set -euxo pipefail

      clone_repo_and_cd "${PUNGI_CONFIG_REPO}" "${PUNGI_CONFIG_REPO_REF}"
      cd pungi-config
      mv koji.conf.d/* /etc/koji.conf.d/

      echo "Tagging packages"
      while IFS= read -r line; do
        koji --profile=cbs --cert "${CBS_ATCBOT_SSL_CERT}" add-pkg --owner=atcbot "${KOJI_PROMOTE_TAG}" "${line}"
      done < "${CI_PROJECT_DIR}"/packages/pkgs.txt

      echo "Tagging builds"
      while IFS= read -r line; do
        koji --profile=cbs --cert "${CBS_ATCBOT_SSL_CERT}" tag "${KOJI_PROMOTE_TAG}" "${line}"
      done < "${CI_PROJECT_DIR}"/packages/builds.txt
  rules:
    - if: $DISABLE_PROMOTE_KOJI_BUILDS == "true"
      when: never
    - !reference [.run_on_schedule]
